#!/usr/bin/perl -w
#
# Copyright (c) 2000-2022 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
# Osload library. Basically the backend to the osload script, but also used
# where we need finer control of loading of nodes.
#
package libosload_edge_100bf;

use strict;
use English;
use Data::Dumper;

#
# Configure stuff
#
my $TB		= "@prefix@";
my $TESTMODE    = @TESTMODE@;

use libosload_new;
use base qw(libosload_pc);
use vars qw($AUTOLOAD);

use libdb;
use emdb;
use EmulabConstants;
use libtestbed;
use libtblog_simple;
use libreboot;

sub New($$$)
{
    my ($class, $parent, $type) = @_;

    my $self = $class->SUPER::New($parent, $type);
    bless($self, $class);

    return $self;
}

sub Reboot($$;$)
{
    my ($self, $nodeobject, $waitmode) = @_;
    if (!defined($waitmode)) {
	$waitmode = 0;
    }
    my $node_id  = $nodeobject->node_id();
    my $pingable = $self->Pingable($nodeobject);

    my %reboot_args  = ();
    $reboot_args{'debug'}      = $self->debug();
    $reboot_args{'waitmode'}   = $waitmode;
    $reboot_args{'nodelist'}   = [ $node_id ];
    $reboot_args{'powercycle'} = !$pingable;
    $reboot_args{'force'}      = 1;

    #
    # No need to look at the failures array, there is only one node
    # and either it works or it does not.
    #
    my %reboot_failures = ();
    if (nodereboot(\%reboot_args,\%reboot_failures)) {
	tberror "$self: Reload($node_id): power cycle failed!\n";
	return -1;
    }
    return 0;
}

#
# Reboot with wait.
#
sub RebootWait($$)
{
    my ($self, $nodeobject) = @_;
    return $self->Reboot($nodeobject, 1);
}

1;
