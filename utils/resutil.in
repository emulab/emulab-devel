#!/usr/bin/perl -w
#
# Copyright (c) 2016-2018, 2022, 2024 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
use strict;
use English;
use Getopt::Std;
use Date::Parse;
use Data::Dumper;
use POSIX;

#
# Configure variables
#
my $TB		 = "@prefix@";
my $TBOPS        = "@TBOPSEMAIL@";

#
# Turn off line buffering on output
#
$| = 1;

#
# Untaint the path
# 
$ENV{'PATH'} = "/bin:/sbin:/usr/bin:";

#
# Testbed Support libraries
#
use lib "@prefix@/lib";
use emdb;
use libtestbed;
use User;
use Project;
use Group;
use Reservation;
use WebTask;
use emutil;
use libEmulab;
use ResUtil;

sub usage()
{
    print STDERR "Usage: resutil ...\n";
    exit(-1);
}
my $optlist    = "dt:r";
my $debug      = 0;
my $webtask_id;
my $webtask;
my $project;
my $group;
my @timeline;
my @reservations;

sub fatal($)
{
    my ($mesg) = $_[0];

    if (defined($webtask)) {
	$webtask->Exited(-1);
	$webtask->output($mesg);
    }
    die("*** $0:\n".
	"    $mesg\n");
}

#
# Parse command arguments. Once we return from getopts, all that should be
# left are the required arguments.
#
my %options = ();
if (! getopts($optlist, \%options)) {
    usage();
}
if (defined($options{d})) {
    $debug = 1;
    ResUtil::DebugOn();
}
if (defined($options{"t"})) {
    $webtask_id = $options{"t"};
    $webtask = WebTask->Lookup($webtask_id);
    if (!defined($webtask)) {
	fatal("Could not lookup webtask $webtask_id");
    }
    # Convenient.
    $webtask->AutoStore(1);
}

if (defined($options{"r"})) {
    my $res = Reservation->Lookup($ARGV[0]);
    if (!defined($res)) {
	fatal("Could not lookup reservation");
    }
    $project = Project->Lookup($res->pid());
    if (!defined($project)) {
	fatal("Could not lookup project for reservation");
    }
    $group = Group->Lookup($res->gid());
    if (!defined($project)) {
	fatal("Could not lookup project for reservation");
    }
    @reservations = ($res);
    my @allres = ResUtil::CollectReservations($project, undef, 0);
    @timeline  = ResUtil::CreateTimeline($project, $group, undef, @allres);
}
elsif (@ARGV) {
    $project = Project->Lookup($ARGV[0]);
    if (!defined($project)) {
	fatal("No such project");
    }
    if (!defined($project)) {
	fatal("Could not lookup project for reservation");
    }
    $group = $project->GetProjectGroup();
    @reservations = CollectReservations($project, $group, undef, 0);
    if (!@reservations) {
	print STDERR "No reservations to process\n";
	if (defined($webtask)) {
	    $webtask->Exited(1);
	    $webtask->output("No reservations to process");
	}
	exit(1);
    }
    @timeline = ResUtil::CreateTimeline($project, @reservations);
}

foreach my $res (@reservations) {
    ResUtil::ReservationUtilizationNew($res, @timeline);
    print STDERR "DONE\n";
    print Dumper($res);
}
exit(0);

